#!/usr/bin/python

import socket
import json
from threading import Thread
from SocketServer import ThreadingMixIn
import os
import sys
import getopt
import time
from rx import Observable, Observer


class Client:
    class Error(Exception):
        def __init__(self, message):
          self.message = message

        def __repr__(self):
            return self.message

        def __str__(self):
            return self.message            

    COMMAND_BUFFER_SIZE = 200
    TIMEOUT = 3

    def __init__(self, host, port):     
        self.host = host
        self.port = port
 
    def send(self, fileName):
        file = open(fileName, 'rb')
        file.seek(0, 2)
        fileSize = file.tell()

        conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        conn.settimeout(self.TIMEOUT)

        try:
            conn.connect((self.host, self.port))

            startTime = time.time()

            while True:
                respData = conn.recv(self.COMMAND_BUFFER_SIZE)
                if not respData:
                    raise Client.Error("Server dont send data")

                #print respData
                resp = json.loads(respData)
                
                if resp['command'] == 'hello':
                    conn.send(json.dumps({'hostname': socket.gethostbyname(socket.gethostname())}))

                if resp['command'] == 'request_action':
                    conn.send(json.dumps({'action': 'upload', 'filename': fileName, 'filesize': fileSize}))                
                
                if resp['command'] == 'get_chunk':
                    offset = resp['offset']
                    size = resp['size']

                    # look at https://pypi.python.org/pypi/pysendfile
                    file.seek(offset, 0)
                    chunk = file.read(size)
                    conn.send(chunk)

                if resp['command'] == 'finish_upload':
                    if resp['status'] == 'error':
                        raise Client.Error(resp['message'])
                    if resp['status'] == 'success':
                        break

                    raise Client.Error(resp['finish with unknown status'])

            finishTime = time.time()
            timeDiff = finishTime-startTime
            print 'Upload speed: %0.3f MB/sec' % (fileSize / (1024*1024) / timeDiff)
        finally:
            conn.close()

    def sendSource(self, fileName):        
        return Observable.from_callback(lambda fileName, cb:
            self.send(fileName)
        )(fileName)


def printf(text):
   print text

def main(argv):
    if len(argv) != 2:
        print argv[0], " <file_to_upload>"
        exit(1)
       
    host = socket.gethostname()
    port = 2004
     
    client = Client(host, port)

    client.sendSource(argv[1]).subscribe(
        on_completed=lambda: printf("Done!"),
        on_error=lambda error: printf("Error " + str(error))
        )


if __name__ == "__main__":
   main(sys.argv)
