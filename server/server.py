#!/usr/bin/python

import socket
import json
from threading import Thread 
from SocketServer import ThreadingMixIn 
import os

class ClientThread(Thread):
    MAX_CHUNK_SIZE = 4096
    COMMAND_BUFFER_SIZE = 200

    def __init__(self, conn): 
        Thread.__init__(self) 
        self.conn = conn        
 
    def run(self): 
        self.conn.send(json.dumps({"command": "hello"}))
        respData = self.conn.recv(self.COMMAND_BUFFER_SIZE)
        print respData
        resp = json.loads(respData)
        hostName = resp['hostname']

        self.conn.send(json.dumps({"command": "request_action"}))
        respData = self.conn.recv(self.COMMAND_BUFFER_SIZE)
        print respData

        resp = json.loads(respData)        

        if resp["action"] == "upload":
            fileName = resp['filename']
            fileSize = resp['filesize']         
            
            head, tail = os.path.split(fileName)
            if not head:
                directory = "downloads/{}/".format(hostName)
            else:
                directory = "downloads/{}/{}/".format(hostName, head)

            finalFileName = "{}{}".format(directory, tail)
            print finalFileName

            try:
                if not os.path.exists(directory):
                    os.makedirs(directory)

                with open(finalFileName, 'wb') as file:
                    chunkCount = fileSize / self.MAX_CHUNK_SIZE

                    for chunkNum in xrange(0, chunkCount):
                        if chunkNum == chunkCount - 1:                        
                            chunkSize = fileSize % chunkCount
                        else:
                            chunkSize = self.MAX_CHUNK_SIZE

                        offset = chunkNum * chunkSize                    
                        self.conn.send(json.dumps({"command": "get_chunk", "offset": offset, "size": chunkSize}))
                        data = self.conn.recv(chunkSize)
                        file.write(data)

                self.conn.send(json.dumps({'command': 'finish_upload', 'status': 'success'}))
            except Exception as ex:
                self.conn.send(json.dumps({'command': 'finish_upload', 'status': 'error', 'message': str(ex)}))
                os.remove(finalFileName)
            finally:
                self.conn.close()

class Server:
    CLIENT_BUFFER_SIZE = 20

    def __init__(self, ip, port): 
        self.ip = ip 
        self.port = port 

    def run(self):
        print "Start server at " + self.ip + ":" + str(self.port) 

        server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)         
        server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) 
        server.bind((self.ip, self.port)) 
        threads = [] 

        server.listen(1) 

        while True:             
            (conn, (ip, port)) = server.accept() 
            print "New server socket thread started for " + ip + ":" + str(port)             
            clientThread = ClientThread(conn)
            clientThread.start() 
            threads.append(clientThread) 

        for t in threads: 
            t.join() 

server = Server('0.0.0.0', 2004)
server.run()
 
